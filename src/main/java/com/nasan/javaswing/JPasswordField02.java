/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class JPasswordField02 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        final JLabel label = new JLabel();
        label.setBounds(20, 150, 200, 50);
        
        final JPasswordField value = new JPasswordField();
        value.setBounds(100, 75, 100, 30);
        
        JLabel label01 = new JLabel("Username:");
        label01.setBounds(20, 20, 80, 30);
        
        JLabel label02 = new JLabel("Password:");
        label02.setBounds(20, 75, 80, 30);
        
        JButton button = new JButton("Login");
        button.setBounds(100, 120, 80, 30);
        
        final JTextField text = new JTextField();
        text.setBounds(100, 20, 100, 30);
        
        frame.add(value);
        frame.add(label01);
        frame.add(label);
        frame.add(label02);
        frame.add(button);
        frame.add(text);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "Username " + text.getText();
                data += ", Password: "+ new String(value.getPassword());
                label.setText(data);
            }
        });
    }
}
