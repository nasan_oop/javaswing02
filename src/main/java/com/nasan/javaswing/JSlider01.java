/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

/**
 *
 * @author nasan
 */
import javax.swing.*;

public class JSlider01 extends JFrame {

    public JSlider01() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JSlider01 frame = new JSlider01();
        frame.pack();
        frame.setVisible(true);
    }
}
