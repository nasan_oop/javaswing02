/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author nasan
 */
public class JLabel01 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("Label Example");
        frame.setSize(300, 300);
        JLabel label01, label02;

        label01 = new JLabel("First Label");
        label01.setBounds(50, 50, 100, 30);
        frame.add(label01);
        
        label02 = new JLabel("Second Label");
        label02.setBounds(50, 100, 100, 30);
        frame.add(label02);

        frame.setLayout(null);
        frame.setVisible(true);
    }
}
