/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author nasan
 */
public class JPasswordField01 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        JPasswordField value = new JPasswordField();
        JLabel label01 = new JLabel("Password:");
        label01.setBounds(20, 100, 80, 30);
        value.setBounds(100, 100, 100, 30);
        frame.add(value);
        frame.add(label01);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
