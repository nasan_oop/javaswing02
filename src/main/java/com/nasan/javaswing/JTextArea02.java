/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author nasan
 */
public class JTextArea02 implements ActionListener {

    JLabel label01, label02;
    JTextArea area;
    JButton button;

    JTextArea02() {
        JFrame frame = new JFrame();
        label01 = new JLabel();
        label01.setBounds(50, 25, 100, 30);
        label02 = new JLabel();
        label02.setBounds(160, 25, 100, 30);
        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);
        button = new JButton("Count Words");
        button.setBounds(100, 300, 120, 30);
        button.addActionListener(this);
        frame.add(label01);
        frame.add(label02);
        frame.add(area);
        frame.add(button);
        frame.setSize(450, 450);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        label01.setText("Words: " + words.length);
        label02.setText("Characters: " + text.length());
    }
    public static void main(String[] args) {
        new JTextArea02();
    }
}
