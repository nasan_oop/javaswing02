/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class JTextField02 implements ActionListener {

    JTextField textField01, textField02, textField03;
    JButton button01, button02;

    JTextField02() {
        JFrame frame = new JFrame();
        textField01 = new JTextField();
        textField01.setBounds(50, 50, 150, 20);
        textField02 = new JTextField();
        textField02.setBounds(50, 100, 150, 20);
        textField03 = new JTextField();
        textField03.setBounds(50, 150, 150, 20);
        textField03.setEditable(false);
        button01 = new JButton("+");
        button01.setBounds(50, 200, 50, 50);
        button02 = new JButton("-");
        button02.setBounds(120, 200, 50, 50);
        button01.addActionListener(this);
        button02.addActionListener(this);
        frame.add(textField01);
        frame.add(textField02);
        frame.add(textField03);
        frame.add(button01);
        frame.add(button02);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s1 = textField01.getText();
        String s2 = textField02.getText();
        int a = Integer.parseInt(s1);
        int b = Integer.parseInt(s2);
        int c = 0;
        if (e.getSource() == button01) {
            c = a + b;
        } else if (e.getSource() == button02) {
            c = a - b;
        }
        String result = String.valueOf(c);
        textField03.setText(result);
    }
    public static void main(String[] args) {
        new JTextField02();
    }
}
