/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class JButton02 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        
        final JTextField textField = new JTextField();
        textField.setBounds(50, 50, 150, 20);
        
        JButton button = new JButton("Click Here");
        button.setBounds(50, 100, 95, 30);
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                textField.setText("Welcome to Javapoint");
            }
            
        });
        frame.add(button);
        frame.add(textField);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
