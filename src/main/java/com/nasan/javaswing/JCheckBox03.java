/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author nasan
 */
public class JCheckBox03 extends JFrame implements ActionListener {

    JLabel label;  
    JCheckBox checkBox01,checkBox02,checkBox03;  
    JButton button;  
    
    JCheckBox03(){
        label=new JLabel("Food Ordering System");  
        label.setBounds(50,50,300,20);  
        checkBox01=new JCheckBox("Pizza @ 100");  
        checkBox01.setBounds(100,100,150,20);  
        checkBox02=new JCheckBox("Burger @ 30");  
        checkBox02.setBounds(100,150,150,20);  
        checkBox03=new JCheckBox("Tea @ 10");  
        checkBox03.setBounds(100,200,150,20);  
        button=new JButton("Order");  
        button.setBounds(100,250,80,30);  
        button.addActionListener(this);  
        add(label);add(checkBox01);add(checkBox02);add(checkBox03);add(button);  
        setSize(400,400);  
        setLayout(null);  
        setVisible(true);  
        setDefaultCloseOperation(EXIT_ON_CLOSE);          
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        float amount=0;  
        String msg="";  
        if(checkBox01.isSelected()){  
            amount+=100;  
            msg="Pizza: 100\n";  
        }  
        if(checkBox02.isSelected()){  
            amount+=30;  
            msg+="Burger: 30\n";  
        }  
        if(checkBox03.isSelected()){  
            amount+=10;  
            msg+="Tea: 10\n";  
        }  
        msg+="-----------------\n";  
        JOptionPane.showMessageDialog(this,msg+"Total: "+amount);  
    }
    public static void main(String[] args) {
        new JCheckBox03();
    }
}
