/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class JLabel02 extends Frame implements ActionListener {
    JTextField textField;
    JLabel label;
    JButton button;
    
    JLabel02(){
        textField = new JTextField();
        textField.setBounds(50, 50, 150, 20);
        label = new JLabel();
        label.setBounds(50, 100, 250, 20);
        button = new JButton("Find IP");
        button.setBounds(50, 150, 95, 30);
        button.addActionListener(this);
        add(button);
        add(textField);
        add(label);
        setSize(400,400);
        setLayout(null);
        setVisible(true);
    }
        
    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            String host = textField.getText();
            String ip = java.net.InetAddress.getByName(host).getHostAddress();  
            label.setText("IP of "+host+" is: "+ip);  
        }
        catch(Exception E){
            System.out.println(E);
        }
    }
    public static void main(String[] args) {
        new JLabel02();
    }

}
