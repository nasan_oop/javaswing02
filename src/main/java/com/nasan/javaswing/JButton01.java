/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author nasan
 */
public class JButton01 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        JButton button = new JButton("Click Here");
        button.setBounds(100, 150, 100, 30);
        frame.add(button);
        frame.setSize(400,400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
