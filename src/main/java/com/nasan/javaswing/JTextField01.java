/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author nasan
 */
public class JTextField01 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("TextField Example");
        JTextField textField01;
        JTextField textField02;
        textField01 = new JTextField("Welcome to Javatpoint.");
        textField01.setBounds(50, 100, 200, 30);
        textField02 = new JTextField("AWT Tutorial");
        textField02.setBounds(50, 150, 200, 30);
        frame.add(textField01);
        frame.add(textField02);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
