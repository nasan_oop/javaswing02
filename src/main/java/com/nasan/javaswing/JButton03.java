/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.javaswing;

import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author nasan
 */
public class JButton03 {

    JButton03() throws MalformedURLException {
        JFrame frame = new JFrame("Button Example");
        JButton button = new JButton(new ImageIcon(new URL("https://cdn-icons-png.flaticon.com/512/5046/5046963.png")));
        button.setBounds(200, 200, 400, 400);
        frame.add(button);
        frame.setSize(1000, 1000);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) throws MalformedURLException  {
        new JButton03();
    }
}
